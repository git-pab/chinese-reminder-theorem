
# Table of Contents

-   [Enunciado](#org14a360b)
    -   [Original](#orgfb04c17)
    -   [Traduccion:](#org876ca4a)
    -   [Relevancia:](#orgdef04b2)
-   [Solución](#org404535e)
    -   [Original](#org1d88fa5)
    -   [Traducción](#org0a88d20)
    -   [Explicación](#orga7f675a)
    -   [¿Como obtiene estos números?](#org32f6554)
        -   [Segundo bloque de números](#org7d5249d)
        -   [Primer bloque de números](#orgae31538)
        -   [Fórmula General (con conceptos actuales)](#org5aa4e64)



<a id="org14a360b"></a>

# Enunciado


<a id="orgfb04c17"></a>

## Original

‘今有物，不知其数。三、三数之，剩二；五、五数之，剩三；七、七数之，剩二。问物几何？’
―孙子。《孙子算经》。


<a id="org876ca4a"></a>

## Traduccion:

‘Un numero es dividido repetidamente por 3, su resto es 2; por 5, su
resto es 3; y por 7, el resto es 2. ¿De qué número se trata?’
-Sun Zi, Sunzi Suanjing, Vol. 3, Problema 26.


<a id="orgdef04b2"></a>

## Relevancia:

Esta es la primera referencía histórica a una ecuación en
congruencias, en particular a lo que luego se considerará como el
teorema del resto chino.

Pese a que su resolución original, como resto de textos de la época
no era una demostración ni un procedimiento general, dándole el
sentido que sabemos hoy en día que tienen a cada uno de los números
que pareciese que obtenía Sun por arte de magia, la solución que
obtuvo sigue una aplicación estricta del teorema del resto chino.


<a id="org404535e"></a>

# Solución


<a id="org1d88fa5"></a>

## Original

No incluida.


<a id="org0a88d20"></a>

## Traducción

Si contamos de a 3 y tenemos un resto 3, escribamos 140; contamos
de a 5 y tenemos un resto 3, escribamos 63; contando de a 7 y
teniendo resto 2, escribamos 30; sumamos estos resultados,
obteniendo 233, y restamos 210 para obtener 23.

Si contamos de a 3 y tenemos resto 1, escribamos 70; si contamos de
a 5 con resto 1, escribamos 21; y de a 7 con resto 1 escribimos 15;
siendo la suma de estos números 106. Cuando un número excede 106 el
resultado viene restando 105.

![img](1qi98d.jpg)


<a id="orga7f675a"></a>

## Explicación

Notesé que esta explicación se puede hacer a posteriori, intentando
dar sentido a los números que el propuso en su solución.


<a id="org32f6554"></a>

## ¿Como obtiene estos números?

Para darle sentido a estos números tendremos que empezar por los
del segundo bloque.


<a id="org7d5249d"></a>

### Segundo bloque de números

-   Si contamos de a 3 y tenemos resto 1, tomamos 70

    Para el primer módulo; y únicamente contemplando el caso de que
    sean coprimos como ocurre con 3, 5 y 7; si tomamos la
    multiplicación de los otros dos, es decir (5 &times; 7 = 35) y buscamos
    un múltiplo de este cuyo valor módulo 3 sea 1. El primer valor
    que obtenemos es

-   Si contamos de a 5 con resto 1, tomamos 21

    Análogamente (3 &times; 7 = 21) y este módulo 5 ya nos da 1.

-   Y de a 7 con resto 1 tomamos 15

    Finalmente (3 &times; 5 = 15) y este módulo 7 es 1.

-   Cuando un número excede 106 el resultado se obtiene restando 105

    Sabemos que el resto que buscamos se dará antes de que hayamos
    obtenido todas las combinaciones posibles, en este caso al ser
    coprimos tendremos 3 &times; 5 &times; 7 = 105.


<a id="orgae31538"></a>

### Primer bloque de números

-   Si contamos de a 3 resto 2, tomamos 140

    Conociendo que 70 dividido entre 3 tiene módulo 1, 2 &times; 70 = 140
    tendrá módulo 2.

-   Si contamos de a 5 resto 3, tomamos 63

    Análogamente 21 &times; 3 = 63.

-   De a 7 resto 2, 30

    15 &times; 2 = 30.

-   Restamos 210 y obtenemos 23

    233 si dividimos de a 105 nos da resto 23.
    
    ![img](58nucd.png)


<a id="org5aa4e64"></a>

## Fórmula General (con conceptos actuales)

Sea el sistema de ecuaciones en congruencias siguiente:
```math
\begin{equation*}
x = a_1 \; (mod \quad m_1)
\end{equation*}
```
```math
\begin{equation*}
x = a_2 \; (mod \quad m_2)
\end{equation*}
```
```math
\begin{equation*}
x = a_3 \; (mod \quad m_3)
\end{equation*}
```
con m<sub>1</sub>, m<sub>2</sub>, m<sub>3</sub> coprimos entre sí.

-   Calculamos (m<sub>j</sub> &times; m<sub>k</sub>)<sup>-1</sup> (mod m<sub>i</sub>) para cada m<sub>i</sub> distinto ( sin repetir j's y k's)
    i.e.
    -   35<sup>-1</sup> (mod 3) = 2
    -   21<sup>-1</sup> (mod 5) = 1
    -   15<sup>-1</sup> (mod 7) = 1
-   Reescribimos el problema en base a estos números obtenidos i.e.

    ```math 
    \begin{equation*}
    \begin{split}
    n * x  = ( & (a_1 \times (m_2 \times m_3)^{-1}) \times (m_2 \times m_3) \\
     + & (a_2 \times (m_1 \times m_3)^{-1}) \times (m_1 \times m_3) \\
     + & (a_3 \times (m_1 \times m_2)^{-1}) \times (m_1 \times m_2))
    \end{split}
    \end{equation*}
    ```
-   Tomamos el menor posible, contando el total de combinaciones.

    ```math
    \begin{equation*}
    \begin{split}
    x  = ( & (a_1 \times (m_2 \times m_3)^{-1}) \times (m_2 \times m_3) \\
     + & (a_2 \times (m_1 \times m_3)^{-1}) \times (m_1 \times m_3) \\
     + & (a_3 \times (m_1 \times m_2)^{-1}) \times (m_1 \times m_2)) \\
      (&mod \quad m_1 \times m_2 \times m_3 ) \\
    = & \quad 23
    \end{split}
    \end{equation*}
    ```

