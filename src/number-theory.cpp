#include "number-theory.hpp"
#include <iostream>
#include <fstream>
#include <stdexcept>

int gcd (int a, int b) {
  if (a == 0)
    return b;
  return gcd(b % a, a);
}

int ext_gcd (int a, int b, int &x, int &y) {
  if (a == 0) {
    x = 0;
    y = 1;
    return b;
  } else {
    int x1, y1;
    int out = ext_gcd(b % a, a, x1, y1);
    x = y1 - (b / a) * x1;
    y = x1;
    return out;
  }
}

int inv_mod (int a, int m) {
  int x,y;
  int gcd = ext_gcd (a, m, x, y);
  if (gcd != 1)
    throw std::invalid_argument("Inverse only exists if coprime.");
  else
    return (x % m + m ) % m;
}

int solve_cong_file (char * file_name) {
  std::ifstream in_file (file_name);
  int num_eq;
  in_file >> num_eq;
  int *reminders = new int[num_eq];
  int *modulus = new int[num_eq];
  //Read file
  for (int i = 0; i < num_eq; i++)
    in_file >> reminders[i] >> modulus[i];
  //Calc \Pi m_i 
  int lcm = 1;
  for (int i = 0; i < num_eq; i++)
    lcm*=modulus[i];
  //Solve Eqn
  int x_factor = 0;
  for (int i = 0; i < num_eq; i++) {
    int b = 1;
    for (int j = 0; j < num_eq; j++)
      if (i != j)
	b *= modulus[j];
    x_factor += reminders[i] * inv_mod (b, modulus[i]) * b;
  }
  return x_factor % lcm;
}
