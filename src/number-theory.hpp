#ifndef NUMBER_THEORY_H
#define NUMBER_THEORY_H
#include <iostream>

int gcd (int a, int b);

int ext_gcd (int a, int b, int &x, int &y);

int inv_mod (int a, int m);

int solve_cong_file (char *file_name);

#endif //NUMBER_THEORY_H
